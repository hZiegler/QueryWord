# QueryWord

# 介绍 #
It's a chrome plugin about querying word.(查询英文单词的chrome插件)
安装此插件之后，Chrome内核的浏览器（支持Chrome，360极速浏览器等等），打开的任何网页，只要能够选中文本，都可以快速使用右键进行查询英文单词。

# 使用步骤： #
1. 下载并安装插件（下面有插件下载地址）。
2. 选中要查询的单词，右键，鼠标移动到“查词”，然后选择想要的词典进行查词。

# 当前支持词典： #
朗文（LONGMAN）、韦氏（MERRIAN）、有道、金山

**使用截图：**

----------

![](https://gitee.com/hZiegler/QueryWord/raw/master/docs/images/2.png)

**配置页面截图**

----------

![](https://gitee.com/hZiegler/QueryWord/raw/master/docs/images/1.png)


# 插件下载： #
[QueryWord-1.0.crx](https://gitee.com/hZiegler/QueryWord/raw/master/plugin.crx "QueryWord-1.0.crx")